/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : pwm

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 24/05/2021 16:47:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for pm_app
-- ----------------------------
DROP TABLE IF EXISTS `pm_app`;
CREATE TABLE `pm_app`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf32 COLLATE utf32_bin NULL DEFAULT NULL COMMENT '名称',
  `logo` varchar(255) CHARACTER SET utf32 COLLATE utf32_bin NULL DEFAULT NULL COMMENT '图标',
  `nick` varchar(255) CHARACTER SET utf32 COLLATE utf32_bin NULL DEFAULT NULL COMMENT '昵称',
  `link` varchar(255) CHARACTER SET utf32 COLLATE utf32_bin NULL DEFAULT NULL COMMENT '链接',
  `description` text CHARACTER SET utf32 COLLATE utf32_bin NULL COMMENT '描述',
  `remark` varchar(500) CHARACTER SET utf32 COLLATE utf32_bin NULL DEFAULT NULL COMMENT '备注',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '状态:1=显示,0=隐藏',
  `delete` tinyint(1) NULL DEFAULT 0 COMMENT '删除:1=已删除,0=未删除',
  `regtime` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `uptime` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf32 COLLATE = utf32_bin COMMENT = '应用' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for pm_pwd
-- ----------------------------
DROP TABLE IF EXISTS `pm_pwd`;
CREATE TABLE `pm_pwd`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `app_id` bigint(20) NULL DEFAULT NULL COMMENT 'APPID',
  `name` varchar(255) CHARACTER SET utf32 COLLATE utf32_bin NULL DEFAULT NULL COMMENT '名称',
  `account` varchar(200) CHARACTER SET utf32 COLLATE utf32_bin NULL DEFAULT NULL COMMENT '账号',
  `email` varchar(100) CHARACTER SET utf32 COLLATE utf32_bin NULL DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(50) CHARACTER SET utf32 COLLATE utf32_bin NULL DEFAULT NULL COMMENT '电话',
  `other` varchar(255) CHARACTER SET utf32 COLLATE utf32_bin NULL DEFAULT NULL COMMENT '其他',
  `pwd` varchar(255) CHARACTER SET utf32 COLLATE utf32_bin NULL DEFAULT NULL COMMENT '密码',
  `remark` varchar(500) CHARACTER SET utf32 COLLATE utf32_bin NULL DEFAULT NULL COMMENT '备注',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '状态:1=显示,0=隐藏',
  `delete` tinyint(1) NULL DEFAULT 0 COMMENT '删除:1=已删除,0=未删除',
  `regtime` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `uptime` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf32 COLLATE = utf32_bin ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for pm_sms
-- ----------------------------
DROP TABLE IF EXISTS `pm_sms`;
CREATE TABLE `pm_sms`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `phone` varchar(50) CHARACTER SET utf32 COLLATE utf32_bin NULL DEFAULT NULL COMMENT '手机号',
  `code` varchar(255) CHARACTER SET utf32 COLLATE utf32_bin NULL DEFAULT NULL COMMENT '验证码',
  `type` enum('1','2','3') CHARACTER SET utf32 COLLATE utf32_bin NULL DEFAULT '2' COMMENT '类型:1=注册,2=登录,3=密码找回',
  `times` int(10) NULL DEFAULT 1 COMMENT '次数',
  `expire` timestamp(0) NULL DEFAULT NULL COMMENT '过期时间',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '状态:1=显示,0=隐藏',
  `delete` tinyint(1) NULL DEFAULT 0 COMMENT '删除:1=已删除,0=未删除',
  `regtime` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `uptime` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf32 COLLATE = utf32_bin ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for pm_user
-- ----------------------------
DROP TABLE IF EXISTS `pm_user`;
CREATE TABLE `pm_user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uid` varchar(20) CHARACTER SET utf32 COLLATE utf32_bin NULL DEFAULT NULL COMMENT '用户名',
  `nick` varchar(20) CHARACTER SET utf32 COLLATE utf32_bin NULL DEFAULT NULL COMMENT '昵称',
  `pwd` varchar(255) CHARACTER SET utf32 COLLATE utf32_bin NULL DEFAULT NULL COMMENT '密码',
  `avatar` varchar(255) CHARACTER SET utf32 COLLATE utf32_bin NULL DEFAULT NULL COMMENT '头像',
  `email` varchar(100) CHARACTER SET utf32 COLLATE utf32_bin NULL DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(50) CHARACTER SET utf32 COLLATE utf32_bin NULL DEFAULT NULL COMMENT '手机号',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '状态:1=显示,0=隐藏',
  `delete` tinyint(1) NULL DEFAULT 0 COMMENT '删除:1=已删除,0=未删除',
  `regtime` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `uptime` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf32 COLLATE = utf32_bin ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;
