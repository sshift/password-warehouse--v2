

/* 发送验证码 t: 1=注册 2=登录 3=密码找回  */
const SMS_TIME = 0
function _sms(p, t = 1) {
    $.ajax({
        type: 'post',
        url: '/index/api/sms',
        data: {phone: p, type: t},
        success: r => {
            if(r.code == 1) {
                _tip(r.msg, 6)
            } else {
                _tip(r.msg, 0)
            }
        },
        error: e => {
            _tip('请求失败', 5)
            console.log(e)
        }
    })
}

/* 提示弹窗 i = 0-6 */
function _tip(t = '', i = -1) {
    if(i == -1) {
        layer.msg(t)
    } else {
        layer.msg(t, {icon: i, offset: '100px', anim: 1})
    }
}

/* post提交请求 */
function _post(u, d, c) {
    $.ajax({
        type: 'post',
        url: u,
        data: d,
        beforeSend: () => {
            load = layer.load(2)
        },
        success: r => {
            setTimeout(() => {
                c && c(r)
            }, 500)
        },
        error: e => {
            _tip('请求失败', 5)
            console.log(e)
        },
        complete: () => {
            setTimeout(() => {
                layer.close(load)
            }, 400)
        }
    })
}

/* get获取 */
function _get(u, c) {
    let load
    $.ajax({
        type: 'get',
        url: u,
        beforeSend: () => {
            load = layer.load(2)
        },
        success: r => {
            setTimeout(() => {
                c && c(r)
            }, 100)
        },
        error: e => {
            _tip('请求失败', 5)
            console.log(e)
        },
        complete: () => {
            setTimeout(() => {
                layer.close(load)
            }, 50)
        }
    })
}

$(document).on('click', '[aiu_link]', function() {
    let u = $(this).attr('aiu_link')
    if(u.length > 0) {
        location.href = u
    }
})

$(document).on('click', '[aiu_open]', function() {
    let u = $(this).attr('aiu_open')
    console.log(u)
    if(u.length > 0) {
        window.open(u, '_blank')
    }
})

$('[aiu_tip]').hover(function() {
    let t = $(this).attr('aiu_tip')
    let th = this
    if(t.length > 0) {
        layer.tips(t, th, {
            tips: 1
        })
    }
}, function() {
    layer.closeAll('tips')
})

$(document).on('click', '[pwd]', function() {
    let pwd = $(this).attr('pwd')
    let txt = '********'
    if($(this).text() == txt) {
        $(this).text(pwd)
    } else {
        $(this).text(txt)
    }
})