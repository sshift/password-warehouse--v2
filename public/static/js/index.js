// 退出
$('.exit').click(() => {
    layer.confirm('是否退出登录?', {icon: 3, title:'提示', anim: 2}, index => {
        _post('/index/api/logout', {}, c => {
            if(c && c.code == 1) {
                location.reload()
            } else {
                _tip('退出失败', 5)
            }
            layer.close(index)
        })
        
    })
})

// 添加app
$('.add').click(() => {
    _get('index/index/add', c => {
        layer.open({
            title: '添加应用',
            type: 1, 
            content: c,
            offset: 'auto',
            area: ['auto', 'auto'],
            fixed: true,
            maxmin: true,
            anim: 2,
        })
    })
})

// 打开编辑
let EDIT = false
$('.edit').click(() => {
    let ele = $('.item-box-edit')
    if(EDIT) {
        ele.hide()
        EDIT = false
    } else {
        //$('.item-box-edit').addClass('flex-box')
        ele.css('display', 'flex')
        EDIT = true
    }
    
})

// 应用删除
$(document).on('click', '.del_app', function() {
    let th = $(this)
    let id = th.attr('id')
    layer.confirm('是否删除?', {icon: 3, title:'提示', anim: 2}, index => {
        if(id.length == 0) {
            _tip('没有ID')
            layer.close(index)
            return
        }
        _post('/index/api/del_app', {id: id}, c => {
            if(c && c.code == 1) {
                _tip('删除成功', 1)
                th.parents('.item').remove()
            } else {
                _tip('退出失败', 5)
            }
            layer.close(index)
        })
    })
})

// 应用编辑
$(document).on('click', '.edit_app', function() {
    let th = $(this)
    let id = th.attr('id')
    if(id.length == 0) {
        _tip('没有ID')
        return
    }
    _get(`index/index/edit?id=${id}`, c => {
        if(c && c.code == -1) {
            _tip(c.msg, 2)
            return
        }
        layer.open({
            title: '编辑',
            type: 1, 
            content: c,
            offset: 'auto',
            area: ['auto', 'auto'],
            fixed: true,
            maxmin: true,
            anim: 2,
        })
    })
})

// 查看密码列表
$(document).on('click', '.pwd', function() {
    let th = $(this)
    let id = th.attr('id')
    if(id.length == 0) {
        _tip('没有ID')
        return
    }
    _get(`index/index/pwd?id=${id}`, c => {
        if(c && c.code == -1) {
            _tip(c.msg, 2)
            return
        }
        layer.open({
            title: '账户列表',
            type: 1, 
            content: c,
            offset: 'auto',
            area: ['auto', 'auto'],
            fixed: true,
            maxmin: true,
            anim: 2,
        })
    })
})


// 删除账户
$(document).on('click', '.pwd_del', function() {
    let th = $(this)
    let id = th.parent().attr('id')
    let par = th.parents('.pwd-item')
    layer.confirm('是否删除?', {icon: 3, title:'提示', anim: 2}, index => {
        if(id.length == 0) {
            _tip('没有ID')
            layer.close(index)
            return
        }
        _post('/index/api/del_pwd', {id: id}, c => {
            if(c && c.code == 1) {
                _tip('删除成功', 1)
                par.remove()
            } else {
                _tip('退出失败', 5)
            }
            layer.close(index)
        })
    })

})

// 编辑账户
$(document).on('click', '.pwd_edit', function() {
    let th = $(this)
    let id = th.parent().attr('id')
    let par = th.parents('.pwd-item')
    if(id.length == 0) {
        _tip('没有ID')
        return
    }
    _get(`index/index/pwd_edit?id=${id}`, c => {
        if(c && c.code == -1) {
            _tip(c.msg, 2)
            return
        }
        layer.open({
            title: '编辑账户',
            type: 1, 
            content: c,
            offset: 'auto',
            area: ['auto', 'auto'],
            fixed: true,
            maxmin: true,
            anim: 2,
            success: function(layero, index) {
                form.render()
                form.on('submit(edit_pwd)', d => {
                    _post('/index/api/edit_pwd', d.field, c => {
                        if(c && c.code == 1) {
                            _tip('编辑成功', 6)
                            layer.close(index)
                            let data = d.field
                            par.find('.name').text(data.name)
                            par.find('.account').text(data.account)
                        } else {
                            _tip(c.msg, 5)
                        }
                    })
                    return false
                })
            }
        })
    })
})

// 查看账户
$(document).on('click', '.pwd_show', function() {
    let th = $(this)
    let id = th.parent().attr('id')
    let par = th.parents('.pwd-item')
    if(id.length == 0) {
        _tip('没有ID')
        return
    }
    _get(`index/index/pwd_show?id=${id}`, c => {
        if(c && c.code == -1) {
            _tip(c.msg, 2)
            return
        }
        layer.open({
            title: '账户详情',
            type: 1, 
            content: c,
            offset: 'auto',
            area: ['auto', 'auto'],
            fixed: true,
            maxmin: true,
            anim: 2,
        })
    })
})