<?php
namespace app\index\controller;

use think\Controller;
use think\Db;
use thnk\facade\APP;

class Index extends Controller
{
    public function initialize() {
        parent::initialize();
    }

    public function __construct() {
        if(!file_exists('../install.lock')) {
            $this->redirect('/install');
        }
    }

    public function index()
    {
        if(!session('?user')) {
            $this->redirect('/login');
        }
        $list = db('app')->where(['delete' => 0, 'status' => 1])->order('uptime', 'desc')->select();
        $tpl = [
            'list'  =>  $list,
        ];
        return view()->assign($tpl);
    }

    // 登录
    public function login() {
        return view();
    }

    // 添加app
    public function add() {
        return view();
    }

    // 编辑app
    public function edit() {
        $id = input('get.id');
        $r = db('app')->where('id', $id)->find();
        if(empty($r)) {
            return Rest([], -1, '无此应用');
        }
        return view()->assign(['d' => $r]);
    }

    // 查看app账户列表
    public function pwd() {
        $id = input('get.id');
        $app = db('app')->where('id', $id)->find();
        if(empty($app)) {
            return Rest([], -1, '无此应用');
        }
        $r = db('pwd')->where([
            'app_id'    =>  $id,
            'status'    =>  1,
            'delete'    =>  0
        ])->order('uptime', 'desc')->select();
        return view()->assign(['list' => $r, 'app' => $app, 'rand' => time()]);
    }

    // 添加app账户
    public function pwd_add() {
        $id = input('get.id');
        $app = db('app')->where('id', $id)->find();
        if(empty($app)) {
            return Rest([], -1, '无此应用');
        }
        return view()->assign(['app' => $app, 'rand' => time()]);
    }

    // 编辑app-pwd
    public function pwd_edit() {
        $id = input('get.id');
        $r = db('pwd')->where('id', $id)->find();
        if(empty($r)) {
            return Rest([], -1, '无此账户');
        }
        return view()->assign(['d' => $r]);
    }

    // 查看app-pwd
    public function pwd_show() {
        $id = input('get.id');
        $r = db('pwd')->where('id', $id)->find();
        if(empty($r)) {
            return Rest([], -1, '无此账户');
        }
        return view()->assign(['d' => $r]);
    }

    public function hello($name = 'ThinkPHP5')
    {
        return 'hello,' . $name;
    }
}
