<?php
namespace app\index\controller;

use think\Controller;
use think\Db;
use thnk\facade\APP;

class Api extends Controller
{
    protected $runtime_start = 0;

    public function initialize() {
        parent::initialize();
        $this->runtime_start = microtime(true);
    }

    /**
     * 如果是否，請將code設置成 < 0的數據，例如-1
     * 
     *  */
    public function Re($data = [], $code = 1, $msg = '请求成功') {
        \header('Content-Type: application/json');
        echo json_encode([
            'data'      =>  $data,
            'code'      =>  $code,
            'msg'       =>  $msg,
            'time'      =>  time(),
            'datetime'  =>  date('Y-m-d H:i:s'),
            'runtime'   =>  round((microtime(true) - $this->runtime_start)*1000, 2).'ms',
            'domain'    =>  getDomain(),
        ], JSON_UNESCAPED_UNICODE); die;
    }

    // 安装
    public function install() {
        if(!request()->isPost()) {
            return Rest([], -1, '非法请求');
        }
        $in = input('post.');
        if(empty($in['uid'])) {
            $this->Re('', -1, '请输入用户名');
        }
        if(empty($in['nick'])) {
            $this->Re('', -1, '请输入昵称');
        }
        if(empty($in['pwd'])) {
            $this->Re('', -1, '请输入密码');
        }
        if($in['pwd'] != $in['pwd2'] ) {
            return Rest([], -1, '两次密码不一致');
        }
        $sql = [
            'uid'   =>  $in['uid'],
            'nick'  =>  $in['nick'],
            'pwd'   =>  sha1($in['pwd']),
        ];
        $d = Db::name('user')->insert($sql);
        if(empty($d)) {
            return Rest([], -1, '安装失败!');
        } else {
            $txt = "
Create Time: ".date('Y-m-d H:i:s')."
Uid: ".$in['uid']."
Nick: ".$in['nick'];
            $res =  file_put_contents('../install.lock', $txt);
            if(empty($res)) {
                Rest([], -1, '写文件失败, 尝试提权');
            }
        }
        return Rest([], 1, '安装成功');

    }
    
    // 登录
    public function login() {
        if(!request()->isPost()) {
            return Rest([], -1, '非法请求');
        }
        $in = input('post.');
        $r = db('user')->where('uid|phone|email', $in['uid'])->where('pwd', sha1($in['pwd']))->find();
        if(empty($r)) {
            return Rest([], -1, '无此用户');
        }
        session('user', $r);
        return Rest([], 1, '登录成功');
    }

    // 退出
    public function logout() {
        session(null);
        if(!session('?user')) {
            return Rest([], 1, '退出成功');
        } else {
            return Rest([], -1, '退出失败');
        }
    }

    // 添加APP
    public function add() {
        // 检测登录
        if(!session('?user')) {
            return Rest([], -1, '还未登录!');
        }
        if(!request()->isPost()) {
            return Rest([], -1, '非法请求');
        }
        $in = input('post.');
        $r = db('app')->where('name', $in['name'])->find();
        if(!empty($r)) {
            return Rest([], -1, '应用已存在');
        }
        $sql = [
            'name'  =>  $in['name'],
            'nick'  =>  $in['nick'],
            'logo'  =>  $in['logo'],
            'link'  =>  $in['link'],
            'description'  => $in['description'],
            'remark'    =>  $in['remark'] 
        ];
        $r = db('app')->insert($sql);
        if(empty($r)) {
            return Rest([], -1, '添加失败!');
        }
        return Rest([], 1, '添加成功!');
    }

    // 删除app
    public function del_app() {
        // 检测登录
        if(!session('?user')) {
            return Rest([], -1, '还未登录!');
        }
        if(!request()->isPost()) {
            return Rest([], -1, '非法请求');
        }
        $in = input('post.');
        $r = db('app')->where('id', $in['id'])->update([
            'delete'    =>  1
        ]);
        if(empty($r)) {
            return Rest([], -1, '删除失败');
        }
        return Rest([], 1, '删除成功');
    }

    // 编辑APP
    public function edit() {
        // 检测登录
        if(!session('?user')) {
            return Rest([], -1, '还未登录!');
        }
        if(!request()->isPost()) {
            return Rest([], -1, '非法请求');
        }
        $in = input('post.');
        // 检测名称是否冲突
        $c = db('app')->where('name', $in['name'])->where('id', '<>', $in['id'])->count();
        if($c > 0) {
            return Rest([], -1, '该应用名已被使用');
        }
        $r = db('app')->update($in);
        if(empty($r)) {
            return Rest([], -1, '编辑失败!');
        }
        return Rest([], 1, '编辑成功!');
    }

    //新增app_pwd
    public function add_pwd() {
        // 检测登录
        if(!session('?user')) {
            return Rest([], -1, '还未登录!');
        }
        if(!request()->isPost()) {
            return Rest([], -1, '非法请求');
        }
        $in = input('post.');
        // 检测名称是否冲突
        $c = db('pwd')->where('name', $in['name'])->where('app_id', $in['app_id'])->count();
        if($c > 0) {
            return Rest([], -1, '该账户名已被使用');
        }
        $r = db('pwd')->insertGetId($in);
        if(empty($r)) {
            return Rest([], -1, '添加失败!');
        }
        return Rest($r, 1, '添加成功!');
    }

    // 删除app_pwd
    public function del_pwd() {
        // 检测登录
        if(!session('?user')) {
            return Rest([], -1, '还未登录!');
        }
        if(!request()->isPost()) {
            return Rest([], -1, '非法请求');
        }
        $in = input('post.');
        $r = db('pwd')->where('id', $in['id'])->update([
            'delete'    =>  1
        ]);
        if(empty($r)) {
            return Rest([], -1, '删除失败');
        }
        return Rest([], 1, '删除成功');
    }

    // 编辑APP_pwd
    public function edit_pwd() {
        // 检测登录
        if(!session('?user')) {
            return Rest([], -1, '还未登录!');
        }
        if(!request()->isPost()) {
            return Rest([], -1, '非法请求');
        }
        $in = input('post.');
        // 检测名称是否冲突
        $c = db('pwd')->where('name', $in['name'])->where('app_id', $in['app_id'])->where('id', '<>', $in['id'])->count();
        if($c > 0) {
            return Rest([], -1, '该应用账户名已存在');
        }
        $r = db('pwd')->update($in);
        if(empty($r)) {
            return Rest([], -1, '编辑失败!');
        }
        return Rest([], 1, '编辑成功!');
    }


}
