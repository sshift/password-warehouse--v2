# 密码仓库_V2

> 2021/04/09 V2.0正式发布

## 介绍
密码太多记不住，用这个就好！

## 安装使用
1. 利用`git clone https://gitee.com/sshift/password-warehouse--v2.git` 下载程序
2. 进入程序目录
3. `composer install`
4. 利用数据库管理工具导入根目录中的 pwm.sql
5. 搭建web环境，创建网站
6. 配置伪静态，参考 [thinkphp5.1](https://www.kancloud.cn/manual/thinkphp5_1/353955)
7. 访问网站进行安装
8. ...

## 遇到问题可以联系qq: 1479221500

## 更新日志

### 2021/04/19
- v2.0发布

### 2021/03/01
- 将老版本代码clone到新项目
- 流程还未走通,敬请期待